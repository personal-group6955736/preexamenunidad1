package com.example.preexamen_01;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ReciboNominaActivity extends AppCompatActivity {

    private TextView lblNombre;
    private EditText etNumRecibo, etNombre, etHorasNormales, etHorasExtra;
    private RadioGroup radioGroupPuestos;
    private RadioButton rbAuxiliar, rbAlbanil, rbIngeniero;
    private TextView tvSubtotal, tvImpuesto, tvTotalPagar;
    private Button btnCalcular, btnCerrar, btnLimpiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);
        lblNombre = findViewById(R.id.txtUsuario);

        etNumRecibo = findViewById(R.id.etNumRecibo);
        etNombre = findViewById(R.id.etNombre);
        etHorasNormales = findViewById(R.id.etHorasNormales);
        etHorasExtra = findViewById(R.id.etHorasExtra);
        radioGroupPuestos = findViewById(R.id.radioGroupPuestos);
        rbAuxiliar = findViewById(R.id.rbAuxiliar);
        rbAlbanil = findViewById(R.id.rbAlbanil);
        rbIngeniero = findViewById(R.id.rbIngeniero);
        tvSubtotal = findViewById(R.id.tvSubtotal);
        tvImpuesto = findViewById(R.id.tvImpuesto);
        tvTotalPagar = findViewById(R.id.tvTotalPagar);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnCerrar = findViewById(R.id.btnCerrar);
        btnLimpiar = findViewById(R.id.btnLimpiar);


        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("usuario");
        lblNombre.setText("Usuario: " + nombre);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etHorasExtra.getText().toString().matches("") ||
                        etHorasNormales.getText().toString().matches("") ||
                        etNombre.getText().toString().matches("") ||
                        etNumRecibo.getText().toString().matches("")){
                    Toast.makeText(getApplicationContext(), "Faltó información", Toast.LENGTH_SHORT).show();
                }
                else{

                calcularTotalPagar();}
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etNumRecibo.setText("");
                etNombre.setText("");
                etHorasNormales.setText("");
                etHorasExtra.setText("");
                tvTotalPagar.setText("Total a Pagar: ");
                tvImpuesto.setText("Impuesto: ");
                tvSubtotal.setText("Subtotal: ");
            }
        });
    }

    private void calcularTotalPagar() {
        int numRecibo = Integer.parseInt(etNumRecibo.getText().toString());
        String nombre = etNombre.getText().toString();
        int horasNormales = Integer.parseInt(etHorasNormales.getText().toString());
        int horasExtra = Integer.parseInt(etHorasExtra.getText().toString());

        int selectedPuestoId = radioGroupPuestos.getCheckedRadioButtonId();
        RadioButton selectedRadioButton = findViewById(selectedPuestoId);
        double factorPuesto = Double.parseDouble(selectedRadioButton.getTag().toString());

        ReciboNomina recibo = new ReciboNomina(numRecibo, nombre, horasNormales, horasExtra, factorPuesto);
        double subtotal = recibo.calcularSubtotal();
        double impuesto = recibo.calcularImpuesto();
        double totalPagar = recibo.calcularTotal();

        tvSubtotal.setText(String.format("Subtotal: %.2f", subtotal));
        tvImpuesto.setText(String.format("Impuesto: %.2f", impuesto));
        tvTotalPagar.setText(String.format("Total a Pagar: %.2f", totalPagar));
    }
}