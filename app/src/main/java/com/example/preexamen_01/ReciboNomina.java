package com.example.preexamen_01;


public class ReciboNomina {
    private int numRecibo;
    private String nombre;
    private int horasNormales;
    private int horasExtra;
    private double factorPuesto;

    public ReciboNomina(int numRecibo, String nombre, int horasNormales, int horasExtra, double factorPuesto) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasNormales = horasNormales;
        this.horasExtra = horasExtra;
        this.factorPuesto = factorPuesto;
    }

    public double calcularPagoBase() {
        double pagoBase = 200; // Pago base constante
        return pagoBase * factorPuesto;
    }

    public double calcularSubtotal() {
        double pagoBase = calcularPagoBase();
        return (horasNormales * pagoBase) + (horasExtra * pagoBase * 2);
    }

    public double calcularImpuesto() {
        return calcularSubtotal() * 0.16; // 16% del Subtotal
    }

    public double calcularTotal() {
        return calcularSubtotal() - calcularImpuesto();
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getHorasNormales() {
        return horasNormales;
    }

    public void setHorasNormales(int horasNormales) {
        this.horasNormales = horasNormales;
    }

    public int getHorasExtra() {
        return horasExtra;
    }

    public void setHorasExtra(int horasExtra) {
        this.horasExtra = horasExtra;
    }
}
