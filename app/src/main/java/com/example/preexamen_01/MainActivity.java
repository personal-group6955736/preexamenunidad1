package com.example.preexamen_01;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private EditText txtUsuario;
    private Button btnIrARecibo;
    private Button btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtUsuario = (EditText) findViewById(R.id.lblUsuario);
        btnCerrar = findViewById(R.id.btnCerrar);
        btnIrARecibo = findViewById(R.id.btnIrARecibo);

        btnIrARecibo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
                startActivity(intent);*/

                if(txtUsuario.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "Faltó capturar el usuario", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent intent = new Intent(getApplicationContext(), ReciboNominaActivity.class);
                    intent.putExtra("usuario", txtUsuario.getText().toString());
                    startActivity(intent);
                }
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}